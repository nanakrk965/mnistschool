﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mnist
{
    public partial class Form1 : Form
    {
        Point prevPos;
        Pen pen;
        Bitmap whiteBoard;
        Bitmap latestPaint;
        Model model;
        Label[] scoreLabels;
        string modelFilePath = "../../frozen_graph";

        public Form1()
        {
            InitializeComponent();
            this.pen = new Pen(Color.Black, 5);
            this.whiteBoard = new Bitmap(280, 280);
            pictureBox1.Image = whiteBoard;

            this.model = new Model(modelFilePath + "1.pb");
            this.scoreLabels = new Label[] {
                score0Label, score1Label,
                score2Label, score3Label,
                score4Label, score5Label,
                score6Label, score7Label,
                score8Label, score9Label,
            };
        }

        private void MouseDownEvent(object sender, MouseEventArgs e)
        {
            this.prevPos = new Point(e.X, e.Y);
        }

        private void MouseDragged(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var canvas = new Bitmap(pictureBox1.Image);
                this.latestPaint = canvas;
                var grp = Graphics.FromImage(canvas);
                grp.DrawLine(this.pen, this.prevPos.X, this.prevPos.Y, e.Location.X, e.Location.Y);
                grp.FillEllipse(Brushes.Black, this.prevPos.X - this.pen.Width / 2, prevPos.Y - this.pen.Width / 2, this.pen.Width, this.pen.Width);
                this.prevPos = new Point(e.X, e.Y);
                pictureBox1.Image = canvas;
            }
        }

        private void RemovePaint(object sender, EventArgs e)
        {
            pictureBox1.Image = this.whiteBoard;
        }

        private bool IsExclusionNum(int i, List<int> exclusionNums)
        {
            foreach (int num in exclusionNums)
            {
                if (num == i)
                {
                    return true;
                }
            }
            return false;
        }

        private int ArgNumOrder(float[] output, List<int> exclusionNums)
        {
            int index = -1;
            for (int i = 0; i < 10; i++)
            {
                if (IsExclusionNum(i, exclusionNums))
                {
                    continue;
                }
                index = i;
                break;
            }
            float value = output[index];

            for (int i = 0; i < 10; i++)
            {
                if (IsExclusionNum(i, exclusionNums))
                {
                    continue;
                }

                float tmpValue = output[i];
                if (tmpValue > value)
                {
                    index = i;
                    value = tmpValue;
                }
            }
            return index;
        }

        private void Predict(object sender, EventArgs e)
        {
            var latestPaintBitMap = new Bitmap(this.latestPaint);
            var resizeMap = new Bitmap(28, 28);
            Graphics graphics = Graphics.FromImage(resizeMap);
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(latestPaintBitMap, 0, 0, 28, 28);
            pictureBox2.Image = resizeMap;

            int h = resizeMap.Height;
            int w = resizeMap.Width;
            float[][][][] inputData = new float[1][][][];

            for (int i = 0; i < 1; i++)
            {
                inputData[i] = new float[28][][];
                for (int j = 0;  j < h ; j++)
                {
                    inputData[i][j] = new float[28][];
                    for (int k = 0; k < w; k++)
                    {

                        inputData[i][j][k] = new float[1];
                        for (int n = 0; n < 1; n++)
                        {
                            int value = resizeMap.GetPixel(k, j).A;
                            if (value != 0)
                            {
                                inputData[i][j][k][n] = 1;
                            }
                            else
                            {
                                inputData[i][j][k][n] = 0;
                            }
                        }
                    }
                }
            }

            var output = model.Predict(inputData);
            var exclusionNums = new List<int>();
            for (int i = 0; i < output.Length; i++)
            {
                for (int j = 0; j < output[i].Length; j++)
                {
                    int index = ArgNumOrder(output[i], exclusionNums);
                    Console.WriteLine(index);
                    exclusionNums.Add(index);
                    string strValue = (output[i][index] * 100).ToString("000");
                    scoreLabels[j].Text = "" + index + "の確率→" + strValue + "%";
                    Console.WriteLine(output[i][index]);
                }
            }
        }

        private void ChangeToModel1(object sender, EventArgs e)
        {
            this.model = new Model(modelFilePath + "1.pb");
        }

        private void ChangeToModel2(object sender, EventArgs e)
        {
            this.model = new Model(modelFilePath + "2.pb");
        }

        private void ChangeToModel3(object sender, EventArgs e)
        {
            this.model = new Model(modelFilePath + "3.pb");
        }
    }
}
